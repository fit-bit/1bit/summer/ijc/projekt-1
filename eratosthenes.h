// eratosthenes.h
// Řešení IJC-DU1, příklad a), 25.2.2019
// Autor: Matěj Kudera (xkuder04), FIT
// Přeloženo: gcc 7.4.0
// Deklarace funkce eratosthenes pro pro rozhraní

#include "bit_array.h"

#ifndef ERATOSTHENES_H
#define ERATOSTHENES_H

void Eratosthenes(bit_array_t pole);

#endif
