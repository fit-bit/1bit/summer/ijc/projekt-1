// eratosthenes.c
// Řešení IJC-DU1, příklad a), 24.2.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// Implementace algoritmu Eratosthenovo síto

#include "bit_array.h"
#include "eratosthenes.h"
#include <stdio.h>
#include <math.h>

void Eratosthenes(bit_array_t pole)
{
	// Nastavení prvočísel v bitovém poli
	for (int i = 2; i <= sqrt(bit_array_size(pole)); i++)
	{
		// zjištrění jestli je číslo prvočíslo
		if (bit_array_getbit(pole, i) == 0)
		{
			// nastavení všech jeho násobků na neprvočísla
			long multiply = 2;
			long index = i * multiply;
			while (index < bit_array_size(pole))
			{
				bit_array_setbit(pole, index, 1);
				multiply++;
				index = i * multiply;
			}
		}
	}
}
