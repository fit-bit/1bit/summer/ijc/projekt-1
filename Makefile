# Makefile
# Řešení IJC-DU1, příklad a),b), 22.2.2019
# Autor: Matěj Kudera, FIT
# Přeloženo: gcc 7.4
# Makefile pro složení programu

# Potřebné parametry
CC= gcc
FLAGS= -O2 -std=c11 -Wall -pedantic
LC_ALL= cs_CZ.utf8
ARCH= -m64 #test 32 a 64 bit

all: primes primes-i steg-decode

############## Výsledné programy ##############
 
# Vytvoření programu primes s makry
primes: primes.o error.o eratosthenes.o
	$(CC) $(FLAGS) $(ARCH) -lm primes.o error.o eratosthenes.o -o primes 

# Vytvoření programu primes-i s inline funkcemi
primes-i: primes-i.o error.o eratosthenes-i.o
	$(CC) $(FLAGS) $(ARCH) -lm primes-i.o error.o eratosthenes-i.o -o primes-i

# Vytvoření programu steg-decode
steg-decode: steg-decode.o error.o ppm.o eratosthenes.o
	$(CC) $(FLAGS) $(ARCH) -lm steg-decode.o error.o ppm.o eratosthenes.o -g -o steg-decode

############ Dependence ##########
# Primes s makry
primes.o: primes.c bit_array.h error.h
	$(CC) $(FLAGS) $(ARCH) -lm -c primes.c -o primes.o

# Primes-i s inline funkcemi
primes-i.o: primes.c bit_array.h error.h
	$(CC) $(FLAGS) $(ARCH) -DUSE_INLINE -lm -c primes.c -o primes-i.o

# steg-decode
steg-decode.o: steg-decode.c error.h ppm.h bit_array.h eratosthenes.h
	$(CC) $(FLAGS) $(ARCH) -lm -c steg-decode.c -g -o steg-decode.o

# error
error.o: error.c error.h
	$(CC) $(FLAGS) $(ARCH) -c error.c -o error.o

# eratosthenes s makry
eratosthenes.o: eratosthenes.c eratosthenes.h bit_array.h error.h
	$(CC) $(FLAGS) $(ARCH) -lm -c eratosthenes.c -o eratosthenes.o

# eratosthenes s inline funkcemi
eratosthenes-i.o: eratosthenes.c eratosthenes.h bit_array.h error.h 
	$(CC) $(FLAGS) $(ARCH) -DUSE_INLINE -lm -c eratosthenes.c -o eratosthenes-i.o

# ppm
ppm.o: ppm.c ppm.h error.h
	$(CC) $(FLAGS) $(ARCH) -c ppm.c -o ppm.o

############ Příkazy ############x

# Příkaz Make run
run:
	ulimit -s 20000
	make
	time ./primes
	time ./primes-i

clean:
	-rm -f primes primes-i steg-decode *.o xkuder04.zip

# Příkaz pro udělání zip archivu
zip: 
	zip xkuder04.zip *.c *.h Makefile
