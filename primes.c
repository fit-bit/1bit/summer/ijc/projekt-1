// primes.c
// Řešení IJC-DU1, příklad a), 21.2.2019
// Autor: Matěj Kudera (xkuder04), FIT
// Přeloženo: gcc 7.4.0
// Program pro otestování struktury bit_array

#include <stdio.h> 
#include "bit_array.h"
#include "eratosthenes.h"

// kvůly nulové optimalizaci
#ifdef USE_INLINE
extern void bit_array_free(bit_array_t);
extern unsigned long bit_array_size(bit_array_t);
extern void bit_array_setbit(bit_array_t, unsigned long, int);
extern int bit_array_getbit(bit_array_t , unsigned long);
#endif // USE_INLINE

int main()
{
	// vytvoření pole
	bit_array_create(test_pole, 123000000);
	
	// převedení na pole prvočísel
	Eratosthenes(test_pole);

	// vytisknutí posledních 10 prvočísel
	int p = 0;
	unsigned long index = bit_array_size(test_pole) - 1;
	while (p < 10)
	{
		if (bit_array_getbit(test_pole, index) == 0)
		{
			p++;
		}
		index--;
	}
	
	while (index < bit_array_size(test_pole))
	{
		if (bit_array_getbit(test_pole, index) == 0)
                {
                        printf("%lu\n", index);
                }
                index++;
	}

	// pro bit_array_alloc	bit_array_free(test_pole);

	return 0;
}
