// steg-decode.c
// Řešení IJC-DU1, příklad b), 21.2.2019
// Autor: Matěj Kudera (xkuder04), FIT
// Přeloženo: gcc 7.4.0
// Testovací program pro modul ppm.c

#include "ppm.h"
#include "error.h"
#include "eratosthenes.h"
#include "bit_array.h"
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
	// Kontrola počtu argumentů
	if (argc != 2)
	{
		error_exit("Program vyžaduje jeden argument, a to obrázek ve formátu ppm.");
	}

	// Načtení obrázku
	struct ppm *picture = ppm_read(argv[1]);
	if (picture == NULL)
	{
		// Teď se vypíšou dvě správy, ale je to v zadání
		error_exit("Chybný formát");
	}

	// Vytvoření bytového pole pro dekódování zprávy
	unsigned long size = picture->xsize*picture->xsize*3;
	bit_array_alloc(test_pole, size);
	
	Eratosthenes(test_pole);

	// Přečtení zprávy pomocí bitového pole

	// Zjištění maximální možné velikosti zprávy
	unsigned long primes = 0;
	for (unsigned long i = 2; i < size; i++)
	{
		if (bit_array_getbit(test_pole, i) == 0)
		{
			 primes++;
		}
	}
	char message[primes];
	memset(message, 0, primes);

	// Získání částí zprávy
	int char_index = 0;
	unsigned long message_index = 0;
	int letter = 0;
	for (unsigned long i = 19; i < size; i++)
	{
		if (bit_array_getbit(test_pole, i) == 0)
		{
			// Nastavení bitu v písmenu podle hodnoty LSB v datech
			if (((picture->data[i]) >> (0) & 1U) == 1)
			{
				letter |= 1UL << (char_index);
			}
			else
			{
				letter &= ~(1UL << (char_index));
			}

			char_index++;

			// Uložení znaku do řetezce
			if (char_index == 8)
			{
				char_index = 0;
				message[message_index] = letter;

				// Kontrola jestli jsme už na konci řetězce
				if (letter == 0)
				{
					break;
				}

				letter = 0;
				message_index++;
			}
		}
	}

	// Kontrola ukončení zprávy v obrázku	
	if (message[message_index] != '\0')
	{
		bit_array_free(test_pole);
        	ppm_free(picture);
		error_exit("Ve zprávě není ukončující znak");
	}

	// Vypsání zprávy
	for (unsigned long i = 0; i <= message_index; i++)
	{
		printf("%c", message[i]);
	}
	putchar('\n');

	bit_array_free(test_pole);
	ppm_free(picture);
	return 0;
}
