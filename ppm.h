// ppm.h
// Řešení IJC-DU1, příklad b), 21.2.2019
// Autor: Matěj Kudera (xkuder04), FIT
// Přeloženo: gcc 7.4.0
// Rozhraní modulu ppm.c

#ifndef PPM_H
#define PPM_H

// struktura pro uložení obrázku
struct ppm {
        unsigned xsize;
        unsigned ysize;
        char data[];    // RGB bajty, celkem 3*xsize*ysize
     };

// Funkce pro načtení obrázku do struktury
struct ppm *ppm_read(const char *filename);
// uvolnění pamětui naalokové funkcí ppm_read
void ppm_free(struct ppm *p);

#endif
