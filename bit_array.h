// bit_array.h
// Řešení IJC-DU1, příklad a), 21.2.2019
// Autor: Matěj Kudera (xkuder04), FIT
// Přeloženo: gcc 7.4.0
// Rozhraní pro datovou strukturu pole bytů

#include "error.h"
#include <math.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#ifndef BIT_ARRAY_H
#define BIT_ARRAY_H

// Typ bitové pole
typedef unsigned long *bit_array_t;


#ifdef USE_INLINE
///////////////////////// Inline Funkce /////////////////////////////

/**
	 uvolní paměť dynamicky alokovaného pole
**/
inline void bit_array_free(bit_array_t jmeno_pole)
{
	free(jmeno_pole);
}

/**
	vrátí deklarovanou velikost pole v bitech (uloženou v poli)
**/
inline unsigned long bit_array_size(bit_array_t jmeno_pole)
{
	return jmeno_pole[0];
}

/**
	nastaví zadaný bit v poli na hodnotu zadanou výrazem
       	(nulový výraz == bit 0, nenulový výraz == bit 1)
       	Př: bit_array_setbit(p,20,1);
**/
inline void bit_array_setbit(bit_array_t jmeno_pole, unsigned long index, int vyraz)
{
	if (index < 0 || index > bit_array_size(jmeno_pole) - 1)
	{
		error_exit("bit_array_getbit: Index %lu mimo rozsah 0..%lu", index, bit_array_size(jmeno_pole) - 1);
	}
	else
	{
		// Nastavení bitu na 0 nebu 1, jestli je hodnota jiná vypíše se error
		if (vyraz == 1)
		{
			long part = floor(index / (sizeof(unsigned long) * 8));
			(jmeno_pole[part + 1]) |= 1UL << (index % (sizeof(unsigned long) * 8));
		}
		else if (vyraz == 0)
		{
			long part = floor(index / (sizeof(unsigned long) * 8));
			(jmeno_pole[part + 1]) &= ~(1UL << (index % (sizeof(unsigned long) * 8)));
		}
		else
		{
			error_exit("Bit lze nastavit jen na hodnotu 0 a 1");
		}
	}
}

/**
	získá hodnotu zadaného bitu, vrací hodnotu 0 nebo 1
       	Př: if(bit_array_getbit(p,i)==1) printf("1");
            if(!bit_array_getbit(p,i))   printf("0");
**/
inline int bit_array_getbit(bit_array_t jmeno_pole, unsigned long index)
{
	if (index < 0 || index > bit_array_size(jmeno_pole) - 1)
	{
		error_exit("bit_array_getbit: Index %lu mimo rozsah 0..%lu", index, bit_array_size(jmeno_pole) - 1);
		return 1; // pro spokojenost překladače
	}
	else
	{
		long part = floor(index / (sizeof(unsigned long) * 8));
		return (jmeno_pole[part + 1]) >> (index % (sizeof(unsigned long) * 8)) & 1U;
	}
}
#else
///////////////////////// Makra //////////////////////////////

/**
	 uvolní paměť dynamicky alokovaného pole
**/
#define bit_array_free(jmeno_pole) free(jmeno_pole)

/**
	vrátí deklarovanou velikost pole v bitech (uloženou v poli)
**/
#define bit_array_size(jmeno_pole) (unsigned long)(jmeno_pole[0])

/**
	nastaví zadaný bit v poli na hodnotu zadanou výrazem
       	(nulový výraz == bit 0, nenulový výraz == bit 1)
       	Př: bit_array_setbit(p,20,1);
**/
#define bit_array_setbit(jmeno_pole, index, vyraz)\
	if (index < 0 || index > bit_array_size(jmeno_pole) - 1)\
	{\
		error_exit("bit_array_getbit: Index %lu mimo rozsah 0..%lu", index, bit_array_size(jmeno_pole) - 1);\
	}\
	else\
	{\
		if (vyraz == 1)\
		{\
			long part = floor(index / (sizeof(unsigned long) * 8));\
			(jmeno_pole[part + 1]) |= 1UL << (index % (sizeof(unsigned long) * 8));\
		}\
		else if (vyraz == 0)\
		{\
			long part = floor(index / (sizeof(unsigned long) * 8));\
			(jmeno_pole[part + 1]) &= ~(1UL << (index % (sizeof(unsigned long) * 8)));\
		}\
		else\
		{\
			error_exit("Bit lze nastavit jen na hodnotu 0 a 1");\
		}\
	}\

/**
	získá hodnotu zadaného bitu, vrací hodnotu 0 nebo 1
       	Př: if(bit_array_getbit(p,i)==1) printf("1");
            if(!bit_array_getbit(p,i))   printf("0");
**/
#define bit_array_getbit(jmeno_pole, index) (int)((index < 0 || index > bit_array_size(jmeno_pole) - 1) ? (error_exit("bit_array_getbit: Index %lu mimo rozsah 0..%lu", index, bit_array_size(jmeno_pole) - 1), 0) : ((jmeno_pole[index / (sizeof(unsigned long) * 8) + 1]) >> (index % (sizeof(unsigned long) * 8)) & 1U))

#endif // DUSE_INLINE
/////////////////////// Funkce které nejdou inline /////////////////////////////

/**
	definuje a _nuluje_ proměnnou jmeno_pole
	(POZOR: opravdu musí _INICIALIZOVAT_ pole bez ohledu na
	to, zda je pole statické nebo automatické/lokální!  Vyzkoušejte si obě
	varianty, v programu použijte lokální pole.)
	Př: static bit_array_create(p,100); // p = pole 100 bitů, nulováno
	bit_array_create(q,100000L); // q = pole 100000 bitů, nulováno
	Použijte static_assert pro kontrolu minimální možné velikosti pole. 
**/
#define bit_array_create(jmeno_pole, velikost)\
	static_assert(velikost > 1, "Špatná velikot pole");\
	long size_in_long = ceil(velikost/(sizeof(unsigned long)*8)) + 1;\
	unsigned long jmeno_pole[size_in_long];\
	memset(jmeno_pole, 0, size_in_long * (sizeof(unsigned long)));\
	jmeno_pole[0] = velikost;

/**
	definuje proměnnou jmeno_pole tak, aby byla kompatibilní s polem
       	vytvořeným pomocí bit_array_create, ale pole bude alokováno dynamicky.
       	Př: bit_array_alloc(q,100000L); // q = pole 100000 bitů, nulováno
       	Použijte assert pro kontrolu minimální možné velikosti pole.
       	Pokud alokace selže, ukončete program s chybovým hlášením:
       	"bit_array_alloc: Chyba alokace paměti"
**/
#define bit_array_alloc(jmeno_pole, velikost)\
	assert(velikost > 1);\
	long size_in_long = ceil(velikost/(sizeof(unsigned long)*8)) + 1;\
	unsigned long *jmeno_pole = malloc(size_in_long * sizeof(unsigned long));\
	if (jmeno_pole == NULL)\
	{\
		error_exit("bit_array_alloc: %s\n", "Chyba alokace paměti");\
	}\
	memset(jmeno_pole, 0, size_in_long * (sizeof(unsigned long)));\
	jmeno_pole[0] = velikost;
					
#endif // BIT_ARRAY_H
