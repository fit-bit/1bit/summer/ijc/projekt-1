// ppm.c
// Řešení IJC-DU1, příklad b), 21.2.2019
// Autor: Matěj Kudera (xkuder04), FIT
// Přeloženo: gcc 7.4.0
// Modul pro uložení RGB obrázku do matice

#include "ppm.h"
#include "error.h"
#include <stdio.h>
#include <stdlib.h>

/**
	načte obsah PPM souboru do touto funkcí dynamicky
        alokované struktury. Při chybě formátu použije funkci warning_msg
        a vrátí NULL.  Pozor na "memory leaks".
**/
struct ppm *ppm_read(const char *filename)
{
	// Otevření obrázku
	FILE *fp;
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		warning_msg("Špatný název obrázku");
                return NULL;
	}

	// Načtení X a Y ze souboru
	long x;
	long y;

	int count = fscanf(fp, "P6\n%ld %ld\n", &x, &y);
	if (count != 2)
	{
		fclose(fp);
		warning_msg("Špatný formát hlavičky souboru s obrázkem.");
		return NULL;
	}

	// Kontrola čísla 255
	int check;

	count = fscanf(fp, "%d\n", &check);
        if (count != 1 || check != 255)
        {
                fclose(fp);
                warning_msg("Špatný formát hlavičky souboru s obrázkem.");
                return NULL;
        }


	// kontrola jestli jsou X a Y platné
	if (x < 1 || y < 1)
	{
		fclose(fp);
                warning_msg("Chyby ve velikosti X a Y v obrázku (nemúže být 0 nebo záporné).");
		return NULL;
	}

	// kontrola maximální velikosti obrázku
	if (3*x*y > 192000000L)
	{
		fclose(fp);
		warning_msg("Maximální velikost obrázku je %d x %d pixelů.", 8000, 8000);
		return NULL;
	}
	
	// Nahrání obrázku do paměti
	struct ppm *picture = malloc(sizeof(struct ppm) + 3*x*y);
	if (picture == NULL)
	{
		fclose(fp);
                warning_msg("Nepovedlo se alokovat paměť.");
                return NULL;
	}


	picture->xsize = x;
	picture->ysize = y;

	// Načtení dat
	int c;
	for (long i = 0; i < 3*x*y; i++)
	{
		c = fgetc(fp);

		// Kontrola správného počtu bajtů
		if (c == EOF)
		{
			free(picture);
			fclose(fp);
                	warning_msg("V obrázku %s není správný počet bajtů na uvedenou velikost %ld x %ld.", filename, x, y);
			return NULL;
		}

		picture->data[i] = c;
	}

	fclose(fp);
	return picture;
}

/**
	uvolní paměť dynamicky alokovanou v ppm_read
**/
void ppm_free(struct ppm *p)
{
	free(p);
}
