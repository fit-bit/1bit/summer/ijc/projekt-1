// error.h
// Řešení IJC-DU1, příklad b), 21.2.2019
// Autor: Matěj Kudera (xkuder04), FIT
// Přeloženo: gcc 7.4.0
// Rozhraní modulu error.c

#ifndef ERROR_H
#define ERROR_H

/**
	Tyto funkce mají
   	stejné parametry jako printf(); tisknou text "CHYBA: " a potom
   	chybové hlášení podle formátu fmt. Vše se tiskne do stderr
   	(funkcí vfprintf) a potom pouze error_exit ukončí program voláním
   	funkce exit(1).
**/
void warning_msg(const char *fmt, ...);
void error_exit(const char *fmt, ...);

#endif
