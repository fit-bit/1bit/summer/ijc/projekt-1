// error.c
// Řešení IJC-DU1, příklad b), 21.2.2019
// Autor: Matěj Kudera (xkuder04), FIT
// Přeloženo: gcc 7.4.0
// Modul pro vypisování chybovích hlášení na stderr

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "error.h"

// Vypíše error na stderr
void warning_msg(const char *fmt, ...)
{
	va_list args;

   	va_start(args, fmt);
	fprintf(stderr,"CHYBA: "); // podle zadání
	vfprintf(stderr , fmt, args);
	fprintf(stderr,"\n"); // TODO snad to tu má být
	va_end(args);
}

// Vypíše error na stderror a ukončí program
void error_exit(const char *fmt, ...)
{
	va_list args;

        va_start(args, fmt);
	fprintf(stderr,"CHYBA: "); // podle zadání
        vfprintf(stderr , fmt, args);
	fprintf(stderr,"\n");
        va_end(args);
	exit(1);
}
